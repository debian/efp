#!/usr/bin/make -f
# Makefile for "Escape From Pong"

version   := 6
tar       := efp-1.$(version).tar.gz
tar_dir   := efp-1.$(version)
zip       := Escape_from_Pong_r$(version).zip
sources   := asm_to_a65.pl efp.asm efp.txt LICENSE makeefp.bat Makefile
xa65_args := -v5 -M
dasm_args := -f3

# Set xa65= or dasm= on the command line to disable autodetection.
dasm := $(shell which dasm)
xa65 := $(shell which xa)

# User targets.
.PHONY: all release clean

# Implementation.
objs :=

all: efp.nes efpbw.nes

ifneq (,$(xa65))
  efp.nes efpbw.nes: %.nes: %.a65
	$(xa65) $(xa65_args) $< -o $@ -l $*.lst
else ifneq (,$(dasm))
  efp.nes efpbw.nes: %.nes: %.asm
	$(dasm) $< $(dasm_args) -o$@ -l$*.lst
else
  $(error Neither xa nor dasm seems available.)
endif
objs += efp.nes efpbw.nes efp.lst efpbw.lst

efpbw.asm: efp.asm
	sed -e '/^THRUST/d' -e 's/^;THRUST/THRUST/' $^ > $@
objs += efpbw.asm

efp.a65 efpbw.a65: %.a65: asm_to_a65.pl %.asm
	perl $^ > $@
objs += efp.a65 efpbw.a65

release: $(tar) $(zip)

$(tar): $(sources)
	tar caf $@ --transform='s|^|$(tar_dir)/|' $^
objs += $(tar)

$(zip): $(sources) $(nes_files)
	zip $@ $^
objs += $(zip)

clean:
	$(RM) $(objs)
